# Testing Script for 42KL C Piscine

A project to crowdsource test cases for C projects in 42KL February 2021 C Piscine.

Motivation behind the project is to relieve the burden of peer evaluators from writing test cases during peer evaluation by providing crowdsourced sample test cases which can be edited by the evaluator for further testing. 

This would free up time for evaluators to discuss program logic and code style.


## Installation

After repository of evaluatee has been cloned, move to root directory of the repository.

Copy the raw link for the `/c_general/42_c_test.sh` and run:
```
curl <link> --output <script_name>
```

## Running the tests

The script runs a Norminette tests with flags `-R CheckForbiddenSourceHeader` on all `.c` files in the exercise directory.

The script will download test cases located in `c_general/tests` which are categorized by the C projects.

The script then compiles all `.c` files with `gcc -Wall -Wextra -Werror` and runs the resulting binary `a.out`.

In order to run the tests, run:
```
sh <script_name> <project_name> <exercise_name> keep
```
where `project_name` is in the format `cXX` and `exercise_name` is in the format `exXX`.

The argument `keep` is optional and if not specified, will delete the main file and binary after tests have been ran.

## Acknowledgments

* Kevin Loh(@kloh_piscine) for help with the script writing.
* Fellow 42KL February 2021 Pisciners for help with the test cases.
* 42KL and 42 for the opportunity

