#include <unistd.h>

void    ft_ultimate_ft(int *********nbr);

/*
** ft_print_number
** - use long to cater for int (2-bytes) value range (-2147483648, 2147483647)
*/

void    ft_print_number(int nb, int n_lead_zero)
{
    long    l_nb;
    int     nb_p1;
    int     nb_p2;
    char    ch;

    l_nb = nb;
    if (l_nb < 0)
    {
        write(1, "-", 1);
        l_nb = l_nb * -1;
    }
    nb_p1 = l_nb / 10;
    nb_p2 = l_nb % 10;
    if (nb_p1 > 0 || n_lead_zero > 1)
    {
ft_print_number(nb_p1, --n_lead_zero);
    }
    ch = '0' + nb_p2;
    write(1, &ch, 1);
}

int     main(void)
{
    int nb;
    int *p_nb1;
    int **p_nb2;
    int ***p_nb3;
    int ****p_nb4;
    int *****p_nb5;
    int ******p_nb6;
    int *******p_nb7;
    int ********p_nb8;
    int *********p_nb9;

nb = 24;
    p_nb1 = &nb;
    p_nb2 = &p_nb1;
    p_nb3 = &p_nb2;
    p_nb4 = &p_nb3;
    p_nb5 = &p_nb4;
    p_nb6 = &p_nb5;
    p_nb7 = &p_nb6;
    p_nb8 = &p_nb7;
    p_nb9 = &p_nb8;
    ft_ultimate_ft(p_nb9);
    ft_print_number(nb, 0);
    return (0);
}
