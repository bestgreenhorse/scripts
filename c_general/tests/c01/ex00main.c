#include <unistd.h>

void    ft_ft(int *nbr);

/*
** ft_print_number
** - use long to cater for int (2-bytes) value range (-2147483648, 2147483647)
*/

void    ft_print_number(int nb, int n_lead_zero)
{
    long    l_nb;
    int     nb_p1;
    int     nb_p2;
    char    ch;

    l_nb = nb;
    if (l_nb < 0)
    {
        write(1, "-", 1);
        l_nb = l_nb * -1;
    }
    nb_p1 = l_nb / 10;
    nb_p2 = l_nb % 10;
    if (nb_p1 > 0 || n_lead_zero > 1)
    {
        ft_print_number(nb_p1, --n_lead_zero);
    }
    ch = '0' + nb_p2;
    write(1, &ch, 1);
}

int     main(void)
{
    int nb;

    nb = 24;
    ft_ft(&nb);
    ft_print_number(nb, 0);
    return (0);
}
