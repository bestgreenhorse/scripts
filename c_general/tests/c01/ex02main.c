#include <unistd.h>

void    ft_swap(int *a, int *b);

void    ft_print_number(int nb, int n_lead_zero)
{
    long    l_nb;
    int     nb_p1;
    int     nb_p2;
    char    ch;

l_nb = nb;
    if (l_nb < 0)
    {
write(1, "-", 1);
l_nb = l_nb * -1;
    }
    nb_p1 = l_nb / 10;
    nb_p2 = l_nb % 10;
    if (nb_p1 > 0 || n_lead_zero > 1)
    {
ft_print_number(nb_p1, --n_lead_zero);
    }
    ch = '0' + nb_p2;
    write(1, &ch, 1);
}

int     main(void)
{
    int nb1;
    int nb2;

nb1 = 24;
    nb2 = 42;
    write(1, &"Before ft_swap: ", 16);
    ft_print_number(nb1, 0);
    write(1, &" ; ", 3);
    ft_print_number(nb2, 0);
    ft_swap(&nb1, &nb2);
    write(1, &"\nAfter ft_swap: ", 16);
    ft_print_number(nb1, 0);
    write(1, &" ; ", 3);
    ft_print_number(nb2, 0);
    write(1, "\n", 1);
    return (0);
}
