#include <stdio.h>
#include <stdlib.h>

int		ft_ultimate_range(int **range, int min, int max);

int   main(void)
{
	int* res;
	int i;
	int size;

	size = ft_ultimate_range(&res, 5, 10);

	printf("Expected: 5, 6, 7, 8, 9\nReturned: ");
	for (i = 0; i < 5; i++)
		  printf("%d, ", res[i]);
	printf("\nExpected return 5, returned %d\n", size);

	free(res);

	printf("Expected: -20, -19, -18, -17, -16\nReturned: ");
	size = ft_ultimate_range(&res, -20, -15);
	for (i = 0; i < 5; i++)
		  printf("%d, ", res[i]);
	printf("\nExpected return 5, returned %d\n", size);
	free(res);

	printf("Expected: -2147483648, -2147483647\nReturned: ");
	size = ft_ultimate_range(&res, -2147483648, -2147483646);
	for (i = 0; i < 2; i++)
			printf("%d, ", res[i]);
	printf("\nExpected return 2, returned %d\n", size);
	free(res);

	printf("Expected: 0, Returned: ");
	size = ft_ultimate_range(&res, 10, 5);
	printf("%x\n", (unsigned int)res);
	printf("\nExpected return 0, returned %d\n", size);
	free(res);
}
