#include <stdio.h>
#include <stdlib.h>

int		*ft_range(int min, int max);

int   main(void)
{
	int* res;
	int i;

	res = ft_range(5, 10);

	printf("Expected: 5, 6, 7, 8, 9\nReturned: ");
	for (i = 0; i < 5; i++)
		if (i != 4)
			printf("%d, ", res[i]);
		else
			printf("%d", res[i]);
	printf("\n");
	free(res);

	printf("Expected: -20, -19, -18, -17, -16\nReturned: ");
	res = ft_range(-20, -15);
	for (i = 0; i < 5; i++)
		if (i != 4)
			printf("%d, ", res[i]);
		else
			printf("%d", res[i]);
	printf("\n");
	free(res);

	printf("Expected: -2147483648, -2147483647\nReturned: ");
	res = ft_range(-2147483648, -2147483646);
	for (i = 0; i < 2; i++)
		if (i != 1)
			printf("%d, ", res[i]);
		else
			printf("%d", res[i]);
	printf("\n");
	free(res);

	printf("Expected: 0, Returned: ");
	res = ft_range(10, 5);
	printf("%x\n", (unsigned int)res);
	free(res);
}
