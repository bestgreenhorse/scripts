#include <stdio.h>
#include <stdlib.h>

char	*ft_convert_base(char *s, char *b1, char *b2);

int main()
{
	char s1[]="   +-++--mihoggli";
	char b1[]="abcdefghijklmno";
	char b2[]="864CBA";
	char s2[]="a";
	char s3[]="-ba";
	char s4[]="-bb";
	char b3[]="abcd";
	char b4[]="wxyz";

	char *c = ft_convert_base(s1, b1, b2);
	printf("Expected: -AAC8C488AAC4\nReturned: %s\n", c);
	free(c);

	c = ft_convert_base(s2, b3, b4);
	printf("Expected: w, Returned: %s\n", c);
	free(c);

	c = ft_convert_base(s3, b3, b4);
	printf("Expected: -xw, Returned: %s\n", c);
	free(c);

	c = ft_convert_base(s4, b3, b2);
	printf("Expected: -A, Returned: %s\n", c);
	free(c);
}
