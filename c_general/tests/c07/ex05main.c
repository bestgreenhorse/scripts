#include <stdlib.h>
#include <stdio.h>

char	**ft_split(char *str, char *set);

int main()
{
	char *s = "This string must not be modified.";
	char **res = ft_split(s, " .");

	int i = 0;
	while (*(res + i) != 0)
	{
		printf("%s\n", *(res + i));
		free(*(res + i));
		i++;
	}
	free(res);

	char *s2 = "\xFF\xFF\xFF\xFF\xFF";
	res = ft_split(s2, "\xFF");

	i = 0;
	while (*(res + i) != 0)
	{
		printf("Empty string test fail! Returned %s\n", *(res + i));
		free(*(res + i));
		i++;
	}
	free(res);

	char *s3 = "I can't believe I missed this case.";
	res = ft_split(s3, "");

	i = 0;
	while (*(res + i) != 0)
	{
		printf("%s\n", *(res + i));
		free(*(res + i));
		i++;
	}
	free(res);

	char *s4 = "";
	res = ft_split(s4, "");

	i = 0;
	while (*(res + i) != 0)
	{
		printf("Empty string test 2 fail! Returned %s\n", *(res + i));
		free(*(res + i));
		i++;
	}
	free(res);
}
