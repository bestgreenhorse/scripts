#include <stdlib.h>
#include <stdio.h>

char *ft_strjoin(int size, char **strs, char *sep);

int main()
{
	char *res;
	char *strs[10];
	char s1[10] = "1";
	char s2[10] = "11";
	char s3[10] = "111";
	char s4[10] = "1111";
	char s5[10] = "11111";
	char s6[10] = "1111";
	char s7[10] = "11";
	char s8[10] = "111";
	char s9[10] = "1";
	char s10[10] = "xd";
	strs[0] = s1;
	strs[1] = s2;
	strs[2] = s3;
	strs[3] = s4;
	strs[4] = s5;
	strs[5] = s6;
	strs[6] = s7;
	strs[7] = s8;
	strs[8] = s9;
	strs[9] = s10;
	char sep[10] = "-";
	
	printf("First test case:\n");
	res = ft_strjoin(0, strs, sep);
	printf("Address: %p\n", res);
	printf("Expected , returned %s\n", res);
	free(res);
	
	printf("Second test case:\n");
	res = ft_strjoin(10, strs, sep);
	printf("Address %p\n", res);
	printf("Expected: 1-11-111-1111-11111-1111-11-111-1-xd\nReturned: %s\n", res);
	free(res);

	printf("Third test case:\n");
	res = ft_strjoin(10, strs, "");
	printf("Address %p\n", res);
	printf("Expected: 1111111111111111111111111xd\nReturned: %s\n", res);
	free(res);

	printf("Bonus test case:\n");
	res = ft_strjoin(-2147483648, strs, sep);
	printf("Address %p\n", res);
	printf("Returned %s\n", res);
	if (res)
		free(res);
}
