#include <stdio.h>

int	ft_atoi(char *str);

int main()
{
	char *s1 = "     --++-++--42";
	printf("Expected -42, got %d\n", ft_atoi(s1));

	char *s2 = "\n\f\r\t\v ++++++42";
	printf("Expected 42, got %d\n", ft_atoi(s2));

	char *s3 = "\t\t\t\t--++-+--++--42-4";
	printf("Expected -42, got %d\n", ft_atoi(s3));

	char *s4 = "-2147483648";
	printf("Expected -2147483648, got %d\n", ft_atoi(s4));
}
