#include <stdio.h>

char *ft_strlowcase(char *str);

int main()
{
    char str[] = "abcgxyzABCLXYZ@{`[ 012789";
    printf("Before lowercase: %s\n", str);
    printf("Returns: %s\n", ft_strlowcase(str));
    printf("After lowercase: %s", str);
}
