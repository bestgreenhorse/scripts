#include <stdio.h>

int	ft_any(char **tab, int (*f)(char *));

int	len_gte_10(char *s)
{
	int	len;

	len = 0;
	while (s[len])
		len++;
	return (len >= 10);
}

int	main()
{
	char	*arr[10] = {
		"a",
		"abc",
		"abcdefg",
		"a",
		"a",
		"a",
		"a",
		"a",
		"abcdefghijklmnop",
		0};

	printf("Expected 1, returned %d\n", ft_any(arr, &len_gte_10));
}
