#include <stdio.h>

int	ft_count_if(char **tab, int length, int (*f)(char *));

int	len_gte_10(char *s)
{
	int	len;

	len = 0;
	while (s[len])
		len++;
	return (len >= 10);
}

int	main()
{
	char	*arr[10] = {
		"a",
		"abc",
		"abcdefg",
		"a",
		"a",
		"a",
		"a",
		"a",
		"abcdefghijklmnop",
		"abcdefghij"};

	printf("Expected 2, returned %d\n", ft_count_if(arr, 10, &len_gte_10));
	printf("Expected 1, returned %d\n", ft_count_if(arr, 9, &len_gte_10));
	printf("Expected 0, returned %d\n", ft_count_if(arr, -2147483648, &len_gte_10));
}
