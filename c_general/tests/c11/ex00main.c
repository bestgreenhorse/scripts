#include <unistd.h>

void	ft_foreach(int *tab, int length, void(*f)(int));

void	ft_putnbr(int a)
{
	char out;

	if (a < 0)
	{
		write(1, "-", 1);
		if (a < -9)
			ft_putnbr((a / 10) * -1);
		ft_putnbr((a % 10) * -1);
	}
	else if (a > 9)
	{
		ft_putnbr(a / 10);
		ft_putnbr(a % 10);
	}
	else
	{
		out = a + '0';
		write(1, &out, 1);	
	}
}

void	ft_puttriple(int a)
{
	ft_putnbr(a * 3);
	write(1, "\n", 1);
}

void	ft_putnegativesquare(int a)
{
	ft_putnbr(-a * a);
	write(1, "\n", 1);
}

int main()
{
	int arr[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	ft_foreach(arr, 5, &ft_puttriple);
	ft_foreach(arr, 8, &ft_putnegativesquare);
}
